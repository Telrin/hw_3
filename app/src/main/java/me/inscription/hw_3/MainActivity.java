package me.inscription.hw_3;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.zip.Inflater;


public class MainActivity extends ActionBarActivity {
    private static final String EXTRA_WEATHER_RESPONSE = "EXTRA_WEATHER_RESPONSE";
    WeatherReadTask weatherReadTask;
    ListView listView;
    List<String> strIconArray;
    List<String> strTemperArray;
    //    List<String> strTimeUTCArray;
    List<String> strTimeLocalArray;
    String weatherResponse;
    Context self;

    public class WeatherArrayAdapter extends ArrayAdapter<String> {

        public WeatherArrayAdapter(Context context, String[] values) {
            super(context, R.layout.weather_layout, values);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) self.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.weather_layout, parent, false);
//            TextView tvWeather = (TextView)rowView.findViewById(R.id.tvWeather);
            ImageView ivWeather = (ImageView) rowView.findViewById(R.id.ivWeather);
            TextView tvTemper = (TextView) rowView.findViewById(R.id.tvTemper);
            TextView tvTimeLocal = (TextView) rowView.findViewById(R.id.tvTimeLocal);
//            TextView tvTimeUTC = (TextView)rowView.findViewById(R.id.tvTimeUTC);
            InputStream ims = null;
            try {
                ims = getAssets().open("icons/" + strIconArray.get(position) + ".png");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Drawable d = Drawable.createFromStream(ims, null);
            ivWeather.setImageDrawable(d);
//            tvWeather.setText(strIconArray.get(position));
            tvTemper.setText(strTemperArray.get(position));
            tvTimeLocal.setText(strTimeLocalArray.get(position));
//            tvTimeUTC.setText(strTimeUTCArray.get(position));
            return rowView;
        }
    }

    void fillStrArray() throws JSONException {
        if (weatherResponse != null) {
            JSONObject jsonObject = new JSONObject(weatherResponse);
            JSONArray forecastList = jsonObject.optJSONArray("list");
            for (int i = 0; i < forecastList.length(); i++) {
                JSONObject forecast = forecastList.getJSONObject(i);
                long longDate = forecast.optLong("dt") * 1000;
                //            Date date = new Date(longDate);
                //            String dateStrFromLong = new SimpleDateFormat("(yyyy-MM-dd) HH:MM", Locale.getDefault()).format(date);
                Calendar calendar = Calendar.getInstance(Locale.getDefault());
                calendar.setTimeInMillis(longDate);
                String dateStrFromLong = DateFormat.format(getString(R.string.str_date_time_format), calendar).toString();
                String dateStr = forecast.optString("dt_txt");
                JSONObject main = forecast.optJSONObject("main");
                double temper = main.optDouble("temp");
                double celsius = temper - 273.15;
                String celsiusStr = String.valueOf(BigDecimal.valueOf(celsius).setScale(0, BigDecimal.ROUND_HALF_UP));
                JSONArray weather = forecast.optJSONArray("weather");
                JSONObject iconCast = weather.getJSONObject(0);
                String icon = iconCast.getString("icon");
                strIconArray.add(icon);
                strTemperArray.add(celsiusStr + " " + "°C");
                strTimeLocalArray.add(dateStrFromLong);
                //            strTimeUTCArray.add(dateStr + " " + "(UTC)");
            }
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (weatherResponse == null) {
            weatherResponse = "";
        }
        outState.putCharSequence(EXTRA_WEATHER_RESPONSE, weatherResponse);
    }

    @Override
    public void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        weatherResponse = savedInstanceState.getCharSequence(EXTRA_WEATHER_RESPONSE).toString();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (weatherResponse == null) {
            weatherReadTask.execute();
        } else {
            try {
                fillStrArray();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (strTemperArray.isEmpty())
                strTemperArray.add(getString(R.string.str_connection_problem));
            String[] values = new String[strTemperArray.size()];
            values = strTemperArray.toArray(values);
            WeatherArrayAdapter adapter = new WeatherArrayAdapter(self, values);
            listView.setAdapter(adapter);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        self = this;
        strIconArray = new ArrayList<>();
        strTemperArray = new ArrayList<>();
        strTimeLocalArray = new ArrayList<>();
        listView = (ListView) findViewById(R.id.listView);
        weatherReadTask = new WeatherReadTask();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent = new Intent(self, WeatherDetail.class);
                intent.putExtra("weatherResponse", weatherResponse);
                TextView tvDate = (TextView) (view.findViewById(R.id.tvTimeLocal));
                String date = tvDate.getText().toString();
                intent.putExtra("selectedTime", date);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onDestroy() {
        if (!weatherReadTask.isCancelled()) {
            weatherReadTask.cancel(true);
        }
        super.onDestroy();
    }

    private class WeatherReadTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
                String query = "http://api.openweathermap.org/data/2.5/forecast?q=Moscow,ru";
                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(query);
                try {
                    HttpResponse response = httpClient.execute(httpGet);
                    HttpEntity httpEntity = response.getEntity();
                    weatherResponse = EntityUtils.toString(httpEntity, HTTP.UTF_8);
                    fillStrArray();
                }
                catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (strTemperArray.isEmpty()) {
                Toast.makeText(self, R.string.str_connection_problem, Toast.LENGTH_LONG).show();
            }
            else {
                String[] values = new String[strTemperArray.size()];
                values = strTemperArray.toArray(values);
                WeatherArrayAdapter adapter = new WeatherArrayAdapter(self, values);
                listView.setAdapter(adapter);
            }
            super.onPostExecute(result);
        }

    }

}
