package me.inscription.hw_3;

import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Locale;


public class WeatherDetail extends ActionBarActivity {

    void fillView(String weatherResponse, String selectedTime) throws JSONException {
        String txtTown = "";
        String txtTemper = "";
        String txtClouds = "";
        String txtPressure = "";
        String txtHumidity = "";
        String txtWind = "";
        String txtIcon = "01d";

        TextView tvTown = (TextView)findViewById(R.id.tvTown);
        TextView tvTemper = (TextView)findViewById(R.id.tvTemper);
        TextView tvClouds = (TextView)findViewById(R.id.tvClouds);
        TextView tvPressure = (TextView)findViewById(R.id.tvPressure);
        TextView tvHumidity = (TextView)findViewById(R.id.tvHumidity);
        TextView tvWind = (TextView)findViewById(R.id.tvWind);
        ImageView ivWeather = (ImageView)findViewById(R.id.ivWeather);

        JSONObject jsonObject = new JSONObject(weatherResponse);
        String city = jsonObject.optJSONObject("city").optString("name");
        String country = jsonObject.optJSONObject("city").optString("country");
        txtTown = city + ", " + country;
        JSONArray forecastList = jsonObject.optJSONArray("list");
        for (int i = 0; i < forecastList.length(); i++) {
            JSONObject forecast = forecastList.getJSONObject(i);
            long longDate = forecast.optLong("dt") * 1000;
            Calendar calendar = Calendar.getInstance(Locale.getDefault());
            calendar.setTimeInMillis(longDate);
            String dateStrFromLong = DateFormat.format(getString(R.string.str_date_time_format), calendar).toString();
            if (selectedTime.equals(dateStrFromLong)) {
                JSONObject main = forecast.optJSONObject("main");
                double temper = main.optDouble("temp");
                double celsius = temper - 273.15;
                txtTemper = String.valueOf(BigDecimal.valueOf(celsius).setScale(0, BigDecimal.ROUND_HALF_UP));
                JSONArray weather = forecast.optJSONArray("weather");
                JSONObject weatherCast = weather.getJSONObject(0);
                txtClouds = weatherCast.getString("main") + ", " + weatherCast.getString("description");
                txtIcon = weatherCast.getString("icon");

                txtPressure = forecast.optJSONObject("main").optString("pressure") + getString(R.string.str_hpa);
                txtHumidity = forecast.optJSONObject("main").optString("humidity") + getString(R.string.str_percent);
                txtWind = forecast.optJSONObject("wind").optString("speed") + getString(R.string.str_meter_per_sec);
                break;
            }
        }

        tvTown.setText(txtTown);
        tvTemper.setText(txtTemper+ " " + "°C");
        tvClouds.setText(txtClouds);
        tvPressure.setText(txtPressure);
        tvHumidity.setText(txtHumidity);
        tvWind.setText(txtWind);

        try {
            InputStream ims = this.getAssets().open("icons/"+txtIcon+".png");
            Drawable d = Drawable.createFromStream(ims, null);
            ivWeather.setImageDrawable(d);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_detail);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        String weatherResponse = getIntent().getExtras().getString("weatherResponse");
        String selectedTime = getIntent().getExtras().getString("selectedTime");
        TextView tvDate = (TextView)findViewById(R.id.tvDate);
        tvDate.setText(selectedTime);
        try {
            fillView(weatherResponse, selectedTime);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
